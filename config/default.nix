{
  imports = [
    ./hardtime.nix
    ./keys.nix
    ./sets.nix

    ./bufferline.nix
    ./colorscheme.nix
    ./lsp.nix
    ./lualine.nix
    ./cmp.nix
    ./luasnip.nix
    ./telescope.nix
    ./staline.nix
    ./whichkey.nix

    ./alpha.nix

    ./noice.nix
    ./nvim-notify.nix

    ./treesitter.nix

    ./nvim-autopairs.nix
    ./ufo.nix
  ];
}
