{
  plugins = {
  lsp = {
    enable = true;
    keymaps = {
      silent = true;
      diagnostic = {
        "<leader>k" = "goto_prev";
        "<leader>j" = "goto_next";
        };
      lspBuf = {
        gd = "definition";
        K = "hover";
        };
    };
    servers = {
      bashls.enable = true;
      clangd.enable = true;
      nil_ls.enable = true;
      tsserver.enable = true;
      pyright.enable = true;
      rust-analyzer = {
        enable = true;
        installCargo = true;
        installRustc = true;
      };
    };
   };
  };
}
